# Week 10 Mini Project
> Oliver Chen (yc557)

## Project Introduction
This project implements a **Rust Serverless Transformer**. We need to dockerize Hugging Face Rust transformer, deploy container to AWS Lambda,and implement query endpoint.

## Project Setup
1. Create a new cargo lambda project using the following command:
```bash
cargo lambda new yc557-week10-mini-project
```
2. Add the following dependencies to `Cargo.toml`.
```toml
[dependencies]
lambda_http = "0.11.1"
tokio = { version = "1", features = ["macros", "rt-multi-thread"] }
tracing = "0.1.27"
log = "0.4.14"
llm = { git = "https://github.com/rustformers/llm" , branch = "main" }
openssl = { version = "0.10.35", features = ["vendored"] }
serde = {version = "1.0", features = ["derive"] }
serde_json = "1.0"
rand = "0.8.5"
```

3. Implement the endpoint in `main.rs`. I used the **pythia-ggml** model with a binary file named `pythia-410m-q4_0-ggjt.bin`. This file can be downloaded from [here](https://huggingface.co/rustformers/pythia-ggml/blob/main/pythia-410m-q4_0-ggjt.bin) from Hugging Face: rustformers - Large Language Models in Rust.

4. Test the endpoint locally by running the following command and visiting http://localhost:9000.
```bash
cargo lambda watch
```
![image](./images/1.png)
![image](./images/2.png)

Or we can use `curl` to test it.
![image](./images/10.jpg)

## Dockerization Using Elastic Container Registry (ECR)
1. Create a new private reposity under ECR in AWS console.
![image](./images/4.png)
2. Connect Docker with AWS using the following command:
```bash
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 471112660632.dkr.ecr.us-east-1.amazonaws.com
```
![image](./images/3.png)
3. Then build the Docker image:
```bash
docker buildx build --progress=plain --platform linux/arm64 -t week10-mini-project .
```
(Some package installation steps ommited)
![image](./images/5.png)
![image](./images/6.png)

4. Lastly, push the Docker image:
```bash
docker push 471112660632.dkr.ecr.us-east-1.amazonaws.com/week10-mini-project:latest
```
![image](./images/7.png)

## AWS Lambda Function
1. Create a new Lambda function using a container image (the previous ECR image).
2. Create a new function URL enabling CORS so that we can access it.
![image](./images/11.jpg)

## CURL Request Against Endpoint
We can use the following command to test the endpoint:
```bash
curl https://ceeqyh4mm4glzhyflexz55qcma0iadcf.lambda-url.us-east-1.on.aws/\?text=Oliver%20starts%20working%20and
```
Here is the result:
![image](./images/12.png)